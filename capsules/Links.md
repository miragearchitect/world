# Links  
This is a list of cool things on the internet

[Home](index.md)

## Cool general things

[Mojeek](https://www.mojeek.com)  
A very nice search engine

## Cool things on Gemini

[Gemini Zine](gemini://gemini.cyberbot.space/smolzine/)  
[Midnight Pub](gemini://midnight.pub)  
[Tilde Team](gemini://tilde.teamgemini://tilde.team)  
[Tilde Pink](gemini://tilde.pink)  
[Tilde Cafe](gemini://tilde.cafe/)  
[Low Earth Orbit Webring](gemini://tilde.team/~khuxkm/leo/)  
[Buletin Board System](gemini://bbs.geminispace.org/)  

[hd.206267.xyz](gemini://hd.206267.xyz)  
Fediverse from Gemini

[CDG](gemini://cdg.thegonz.net/)  
Find Gemini capsules

[Remini](gemini://gemini.bunburya.eu/remini.gmi)  
Gemini Reddit front end

[Kwiecien](gemini://kwiecien.us/)  
Gemini radio and other stuff.

### Nice Capsules

[Federal](gemini://federal.cx/)  
[~solum](gemini://rawtext.club/~sloum/)  
[Nytpu](gemini://nytpu.com/gemlog)  

### Aggregators
[Cosmos](gemini://skyjake.fi/~Cosmos/)  
[Antenna](gemini://warmedal.se/~antenna/)  
[CAPCOM](gemini://gemini.circumlunar.space/capcom/)  
[🧇 NewsWaffle](gemini://gemi.dev/cgi-bin/waffle.cgi/)  

## Web

[I know what you torrent](https://iknowwhatyoudownload.com/en/peer/)  

[Terms of Service; Didn't Read](https://tosdr.org/)  

[Memory League](https://memoryleague.com/#!/home)

[Permacomputing](https://permacomputing.net/)

[chinese-tools](https://www.chinese-tools.com/learn/characters)
Learn Chinese

[Kana.pro](kana.pro)
Learn hiagana and katakana 🈁 (Japanese)

[Learn Awesome](https://learnawesome.org/)

[Math Problem Generator Wolfram Alpha](https://www.wolframalpha.com/problem-generator/)

[Free game deals/discounts](https://gg.deals/news/freebies/)

### Blogs
[unixsheikh](https://unixsheikh.com/)

[SizeOf Cat](https://sizeof.cat/) - [Tor](http://sizeofaex6zgovemvemn2g3jfmgujievmxxxbcgnbrnmgcjcjpiiprqd.onion/)
Privacy, Security, Retro Computing, Vulnerabilities, Exploits, Programming, Link directory

[DigDeeper](http://digdeeper.club/) - [Onion1](http://5essxguxi5enurgtuquvrjuvikss4gc5lbhmtz57cq4cedqx5tqvaxqd.onion/) [I2P](http://r7mv4w5dlcpha4sr5oseugwulntbuds7l6wzvszzformlyhutdtq.b32.i2p/) 
Conspiracy theories, Society, Privacy

[<(^.^)> tsuki](https://tsk.bearblog.dev)

### Articles

[Self-hosting mail isn't hard](https://poolp.org/posts/2019-08-30/you-should-not-run-your-mail-server-because-mail-is-hard/)

### Torrenting

[Linux Games](https://freelinuxpcgames.com/)
Linux game torrents

[Download ebooks (Anna'a Archive)](http://annas-archive.org/)

### Light Novel and Web Novel downloading websites
[A place to download translated Light Novels](justlightnovels.com)  
[And another one with less content](https://armaell-library.net/)  
[Another one, less searchable](https://rektnovelcompilations.wordpress.com/)  
[Lnwnepubs](https://lnwnepubs.wordpress.com/)  

[Armaell's Library](https://armaell-library.net/)   
Light novels

**Web novel  sites**

[RoyalRoad](https://www.royalroad.com/home)  
[ScribbleHub](https://www.scribblehub.com/)  
[Rainobu](https://rainobu.com/)  


## YouTube channels

[System Maid](https://www.youtube.com/channel/UCUMLVgNWusND2ohZuRTmO3g) - [Peertube]
Tech Minetest, and [[VTuber]], [[Maid]]

[Acerola](https://www.youtube.com/channel/UCQG40havu4kNpB4pxUDQhYQ) 
Technically an artist

[jdh](https://www.youtube.com/channel/UCUzQJ3JBuQ9w-po4TXRJHiA) 
Guy doing weird tech stuff

[schnee](https://www.youtube.com/@schnee1) 
Writing analysis