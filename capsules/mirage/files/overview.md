# The Mirage Order

As stated at the top of this transmition, The Mirage Order is a organisation
hiding in the shadow.

The Mirage Order's Headquarters are located in a Universe called by them
#0 : nickname NULL, on Planet Terra.

Deep in the mountains, there is a facility where it all started, The Mirage Architect 
is the founder of the organisation. It is said that he is a genious young man that
proved the existance of the the multiverse. Later he discovered a way of contacting
and travelling between universes. After his first trip, he attained supernatural powers
which put the base for the organisation.

```
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣟⠛⣧⣻⣿⣿⣿⣿⣿⣿⣿⣿⢿⣿⡦⠄⠒⠒⠒⠀⠀⠀⠂⠀⠀⠀⠀⠀⠈⠀⠀⠀⠈⠙⠛⠿⣿⣶⣤⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣟⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋⢀⣀⣀⣦⡄⢀⣤⣤⣤⣤⣴⣶⣆⣀⣴⣲⣾⣿⣟⡲⠤⣀⠀⠀⠂⠄⠀⠀⠈⠙⠻⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡓⣫⣶⣿⣿⣿⣿⣷⣾⣿⣿⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣥⣾⢄⡀⠀⠀⠀⠀⠀⠀⠀⠂⠄⢈⠉⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢟⣡⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣞⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣴⣭⣗⣦⣤⣤⣶⣤⣄⡀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⠋⠀⠛⠛⠻⢿⠟⢻⣿⢟⡿⣿⣿⣏⣴⣿⣿⣿⣿⣿⣿⣿⣿⡿⢿⣿⣿⣿⣿⣦⡻⣿⣿⣿⣯⣽⣿⣿⠉⠙⠿⠁⣡⠟⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⡛⠀⠀⠀⠀⠀⠀⠀⠀⢸⡇⠉⠺⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣟⣫⣼⣿⣿⣿⣋⣿⣿⣾⣿⣿⣿⡬⠿⢷⣤⣿⠿⣷⣦⣾⣿⣿⣿⣿⣿⣿⠋⠃⠀⠀⠀⣿⣿⣿⣿⠿⠿⢙⣋⠀⠙⠻⢿⣿⣿⣿
⠻⠉⠉⢷⠆⣶⣦⣤⢀⡀⠀⠈⡇⠀⠀⠀⢸⣿⣿⣟⡻⠿⣿⣛⠿⣿⣿⣿⣿⣿⣿⠳⠙⠉⢀⣀⣀⡀⡀⠀⢀⠀⠀⠁⠘⢱⣿⣷⣿⣿⠛⠻⠟⡋⠀⠀⠀⠀⠀⣿⣿⡏⠴⠰⠟⠘⠋⠀⠀⠀⠀⠈⠻⣿
⠀⠀⠀⠛⠃⠻⠆⠶⠸⠅⠀⠀⠁⠀⠀⠀⢸⣯⣭⣿⣛⣛⠒⢶⡄⠀⠙⡿⠿⡟⢉⣤⣄⣈⠐⠏⠞⠉⣉⣉⢈⡉⠒⠐⣤⣤⡈⢹⠟⠋⠀⠀⣋⠀⠀⠀⠀⠀⠀⣿⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠶⠦⠤⣬⣭⣭⣹⡇⠛⠂⠀⠀⠀⠀⠛⠈⠛⠁⠶⠆⠀⠀⠀⠀⠀⠀⠒⠌⡏⠀⢸⠀⠀⠀⢘⡃⠀⠀⠀⠀⠀⠀⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣿⣦⣠⢀⡀⠀⢠⠀⠀⠀⠀⠀⠀⡀⠀⠉⠈⡰⠶⠶⠶⠶⠶⠲⡇⠀⠀⠀⠠⠀⢀⠀⠀⠠⠀⢰⢠⡄⠀⠀⠀⠀⠀⠀⡀⡇⠀⢴⠀⠀⠀⢈⠀⠀⠀⠀⠀⠀⢰⣿⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣿⣿⣿⣿⣷⣄⠈⡆⠀⠀⠀⠀⢸⠁⡞⠋⠙⢛⣛⣻⣿⣟⣉⣩⣿⣠⡄⠀⣤⠀⢀⣀⠠⠤⠀⠘⠈⠑⠒⠒⠶⠶⠖⠀⠀⣇⠀⣸⠀⠀⠀⢠⡀⠈⠉⡂⢨⡆⢸⡟⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣿⣿⣿⣿⣿⣿⣀⣇⣀⣤⠀⠀⢸⠀⠃⢸⢸⣿⣏⣭⣭⡬⠿⢗⣿⣿⣿⠿⠟⠃⠘⢁⠀⢀⢀⡨⠀⠀⠀⠀⠀⠀⠀⠒⠀⡀⠀⠿⣶⣦⠀⠲⢦⡀⠀⡇⢸⣿⣼⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠆⠀⣼⡄⢠⣿⣸⣿⣿⣷⡶⢾⠛⠋⠉⠁⠀⠀⠀⡀⢀⢞⣿⣾⠃⠀⠀⠀⠀⠀⠀⠀⠀⠘⠿⡦⠀⠀⠈⠙⢶⣿⣶⣄⣠⡇⣸⣿⣿⣿⣧⣠⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⣿⣿⣿⣿⣿⣿⣿⣽⣶⣄⣰⣄⣺⡇⢸⣿⣹⣏⡉⠀⠀⠀⠑⣄⡀⢀⣨⣼⢟⣣⣾⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁⠀⡀⠀⠀⠀⠈⠛⢿⣿⡇⡟⣿⣿⣿⣿⣿⣿⣿⣾⣷⣶⣶⣤⣤⣤⣤⣤⣴
⣿⣿⣿⣿⣿⣿⣿⣿⠿⠟⠛⠉⠻⣷⣌⠻⣟⣿⣿⣶⠷⠂⣴⣵⣾⢿⣯⣶⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠠⠄⠀⣿⢦⠀⠀⠀⠀⣨⣼⣇⠃⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⠿⠟⠛⠉⠉⠀⠀⠀⠀⠀⠀⠀⠘⢿⣷⣌⣿⣿⣷⣒⣿⣿⣿⠿⠿⠻⠿⠿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⠀⠀⣁⠀⢀⣹⣾⡿⠛⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣶⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡠⣴⣾⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠀⠀⠉⢭⣿⣿⣿⠟⢉⣴⣞⠭⠋⠁⠉⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣧⠀⠀⠀⠀⣀⣤⣶⣾⣷⣾⣿⣵⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠿⣿⢿⣿⣷⣾⣿⠟⠁⠀⠀⠀⠀⠀⠠⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⢧⠹⣿⡦⠖⠒⣿⣿⣿⣿⣿⣿⣿⣿⣽⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⢀⠈⠢⡑⢌⡻⢿⣿⣿⣿⣿⣿⣿⣿
⠖⠋⠁⠀⣀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣍⡉⢁⠐⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣤⣀⣲⣐⣿⣿⣻⣿⣿⣷⣄⢀⢈⠢⡢⡑⢌⠪⡑⢍⠢⣝⣿⣿⣿⣿⣿⣿
⠒⠀⠀⢀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣦⣤⣄⣠⣀⣀⠀⠀⠀⠀⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡠⡑⣔⢌⠢⡑⢌⣶⣿⣿⣿⣿⣿⣿⣿⣿
```
> A tipical hallway of a The Mirage Order facility


## A short description
The Mirage Order's grand goal is to explore the multiverse and study it.
The goal is noble, but the way The Order is trying to accomplish it are not.

Currently, The Mirage Order is made of 503 facilityes in 164 Universes, a more
up to date number can be found on the index of this data.

[Statistics](mirage/index.md)