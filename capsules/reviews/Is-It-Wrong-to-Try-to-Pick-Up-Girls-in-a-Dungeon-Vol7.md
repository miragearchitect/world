# Is It Wrong to Try to Pick Up Girls in a Dungeon?, Vol. 7
📘 **Light Novel**

🙃**Author:** Fujino Omori 

🗓️**Started reading:** 2023-08-18 17:23

🗓️**Finished reading:**  2023-08-19 09:36

💛 **Rating:** 9.8/10 🐇

## Opinion
Hehehe
 
As always, the distance is still there but here we got another problem, scene switching.

There are a lot of POVs in this volume and the switching is very annoying. Besides this, this is my favorite volume of the series so far.
I am waiting to reach the one with the monster girl, the 3rd series in the anime that I watched 2 years ago. I started with the 3rd one then 1st and 2nd.

I don't think that making Hirohime kind of a prostitute was a good choice, I think leaving some real trauma on her would have been better, but I digress.

I like the part where Bell convinces Hirohime to be saved, it super emotional, the book almost made me cry, which is hard to pull of in a book.


I will just say, happy night hunting(what's this supposed to mean)
Ariel, hunting out!🙃