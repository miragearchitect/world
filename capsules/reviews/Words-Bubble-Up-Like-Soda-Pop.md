# Words Bubble Up Like Soda Pop

📘 **Light Novel**

🙃**Author:** Kyohei Ishiguro

🗓️**Started reading:** 2023-09-09 09:38

🗓️**Finished reading:**  2023-09-15 07:46

💛 **Rating:** 8.1/10 🎴


## Opinion
Yey, gonna start with a quote, I did quotes and notes so that I have a better review.

> “Cherry” is my nickname. Someone gave it to me because my last name happens to be Sakura, which means cherry blossom, and before I knew it, I was Cherry and that was that.

Now, the bad stuff...

Firstly, the character are introduced forcefully, if I haven't watched the video essay with the title that went something like this

> why light novels don't have to be good

I wouldn't have ever went past the introduction. It's very 'cringy', it's like hey, that A-san then that's b-kun and that girl over there is S-chan. It basically a list of characters and some facts about them.


The characterisation of the characters are decent trough.

> “Okay! Are all you good little kids ready? Ahhh, but you babies might not understand what I’m saying yet…”
--
What the hell?

Yeah, I don't have anything else to add, it was pretty random. Next.

Now, the current location of the action is also rather confusing. At the start, the protagonist is looking for someone and at some point I think he went out to search for him, very confusing.

Next up, some dialogue is very cringe, did the editor forget to edit the book or something?

The story also makes some scenes that are pretty short and out of context, while also writeing is such a basic way that you sometimes forget who the POV character is.


Now, the question I had in the first place, why does he work there, gets answered only in the middle of the story.


The idea is certainly good, and most of the book is written well, but I think the editor is missing. 

The emotional payload is pretty good actually, but the only thing that is bad is the execution, if you can get over that, it's a good read.

Let's end with a haiku.

> Cherry @ Haiku account
Leaves hidden among
the boundless mountain blossoms
I love you so much
#haiku