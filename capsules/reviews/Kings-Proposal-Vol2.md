# 👑 King's Proposal Vol. 2
Author: Koushi Tachibana   

📅 Date: 2023-07-02  

⭐ Rating:9.7/10👑  

## Opinion
Again, i really enjoyed this book, it very nice. I love how Tachibana-san made this volume a mix of battle and slice of life with a good amount of fun in it. I really like how he is writing. I think i should read his Date a Live series as i have only watched that anime and even that not completely.

In general, it's something i would recommend reading, but it's not a life changing novel like how my current favorite is. Tomorrow i will die, You will revive. 


## Summary
At the start if the volume, we are greeted by  a livestreamer Clara Tokishima on MagiTube. In the first chapter Mukishi Kuga (that also has the body of Saika Kuozakj) was training his substantiations with Kuroe Karasuma, he then learns about the MagiTube and gets to meet Clara by catching her while she was falling from tje sky, Ruri, his sister tells him about how he got assigned by the AI to participate in the following competition between magic schools.
He then meets the other school headmaster and students and gets to participate in the battle as Saika.

Clare suddenly declares love war on Saika over Mukishi and they battle in cooking and seduction, with Saika winning.

In the last chapters, its revealed that Clare fell under the rule of the lind sealed demon Ouroboros, that is immortal and she causes chaos.

Rumi kinda fids out that Mukishi is Saika when he kisses Clare to transform into Saika and kill her. Hee kills her but she still escapes and makes a last livestream on MagiTube 

