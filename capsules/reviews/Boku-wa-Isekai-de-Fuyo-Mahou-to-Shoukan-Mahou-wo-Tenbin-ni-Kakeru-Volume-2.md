# Boku wa Isekai de Fuyo Mahou to Shoukan Mahou wo Tenbin ni Kakeru Volume 2

Author: Yokotsuka Tsukasa

📅 Date: 2023-06-28
🏆 Ranking: 4.5/10 🍇

## Opinion
This second volume is a bit better written, the end's emotional part is pretty good.
The thing that's nice here are the battle scenes and their pacing.
I guess its a pretty good one, nit recommend.