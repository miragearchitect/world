# Is It Wrong to Try to Pick Up Girls in a Dungeon?, Vol. 6
📘 **Light Novel**

🙃**Author:** Fujino Omori 

🗓️**Started reading:** 2023-08-17 21:41

🗓️**Finished reading:** 2023-08-18 17:12

💛 **Rating:** 9.6/10 🐇

## Opinion
Hehe, this one happy.

While the distance problem 😕 is still here, this volume brought more excitement.

The emotional parts almost made me cry, the one with Lilly and there is another one with Bell.

The relationship between Aiz and Bell is very intriguing, leaving me to wonder what dose she really feel for him. 

This novel knows how to turn a harem anime into a more wholesome inocent harem romance, where you often wonder if the heroines love him or are just captured by his cuteness and nativity.

The war plot was entertaining, but the gods keep me in the edge a little bit too much as you can't ever know when some are allies or enemies.


The white 🐇 rabbit Ariel
Is jumping out!