[Index](index.md)

# Poems

## The snow thief
In a little village in the north,  
On a mountain that reaches for the heavens,  
Lives the snow thief.  

On cold nights,  
When the snowflakes spill from the clouds,  
The snow thief scared the clouds,  
To his mountain they go.  

## Dreams
I see a little butterfly,  
His only dream  
To fly,  
From one flower to another  
In his short little life.  

Today another bird will see the light,  
Slowly it will be tought to fly,  
To sing  
And to have fun.  
In the short life  
It lived so far.  

But unlike them,  
That human child  
His life many times the bird's or butterfly's  
Cries in a corner of his room  
His life heading towards doom.  

Even with human's longer life  
Dreams for him hardly comeby.  
And even when,  
Human got some  
Falte likes to have it's fun.  

For some,  
Dreams come and go  
Like river's waters  
Reality forces them to go.  

But for a little  
Unfortunate soul  
Dreams overflow  
Confusion takes it's roots in him.  
Leaving his brain to overheat.  