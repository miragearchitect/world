{ lib, fetchurl, gnumake, gnused, scdoc, go, buildGoModule }: 

buildGoModule rec {
  pname = "gemgen";
  version = "v0.6.0";

  vendorHash = "sha256-KBG7RvWb0nzW9ZSPL1a65Jq9DsSHea8bKLvz6Ft5FsY=";

  src = fetchurl {
    url = "https://git.sr.ht/~kota/gemgen/archive/${version}.tar.gz";
    hash = "sha256-6gq4+0XYsqqJuz1f1OPM9VncUJvmv0wqLLqpXR9p3CI=";  
  };

  nativeBuildInputs = [ gnumake gnused scdoc go ];


  #buildPhase = ''
  #  go build -ldflags "-X main.Version=${version}"
  #  scdoc < gemgen.1.scd | sed "s/VERSION/${version}/g" > gemgen.1
  #  '';

  #postInstall = ''
  #  installManPage gemgen.1
  #'';
  #installPhase = ''
  #make install
  #'';

  meta = with lib; {
    description = "Command line tool for converting Commonmark Markdown to Gemtext. Gemgen uses the goldmark markdown parser and my gemtext rendering module.";
    license = licenses.gpl3;
  };
}

